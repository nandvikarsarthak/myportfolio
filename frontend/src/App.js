import { Flex } from '@chakra-ui/react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Footer from './components/Footer';
import Header from './components/Header';
// import DownloadButton from './components/Button';
import HomeScreen from './screens/HomeScreen';
import SewingScreen from './screens/SewingScreen';
import RST from './screens/RstScreen';
import PortFolio from './screens/PortScreen';
import LogicLoop from './screens/LogicScreen';
import CRUDScreen from './screens/CRUDScreen';

const App = () => {
  return (
    <BrowserRouter>
      <Header />

      <Flex
        as='main'
        mt='72px'
        direction='column'
        py='6'
        px='6'
        bgColor='white.100'>
        <Routes>
          <Route path='/' element={<HomeScreen />} />
          <Route path='/RstScreen' element={<RST />} />
          <Route path='/SewingScreen' element={<SewingScreen />} />
          <Route path='/Portfolio' element={<PortFolio />} />
          <Route path='/LogicLoop' element={<LogicLoop />} />
          <Route path='/CRUDScreen' element={<CRUDScreen />} />
        </Routes>
      </Flex>
      <Footer />
    </BrowserRouter>
  );
};

export default App;
