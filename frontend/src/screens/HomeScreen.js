import {
  Flex,
  Card,
  Box,
  CardBody,
  Text,
  Image,
  Stack,
  Heading,
  SimpleGrid,
} from '@chakra-ui/react';
import { Link as RouterLink, Link } from 'react-router-dom';
// import ProductCard from '../components/ProductCard';
// import products from '../products';

const HomeScreen = () => {
  return (
    <>
      <Flex justifyContent='space-around'>
        <Image
          maxW='sm'
          marginBottom='5'
          // borderRadius='full'
          boxSize='500px'
          marginTop='50px'
          src='assets/ProfilePictures/Screenshot_20240218_224702.png'
          alt='Profile Picture'
        />
        <Box maxW='50rem' paddingTop='50px'>
          <Heading mb={5} fontSize='6xl'>
            Sarthak Ganesh Nandvikar
          </Heading>

          <Link>Email: nandvikarsarthak@gmail.com</Link>
          <Text>Phone: +91 7039967009</Text>

          <br></br>
          <Text fontSize='25px'>
            <strong>Full Stack Web Developer</strong>
          </Text>
          <Text fontSize='xl'>
            I am a proficient Full Stack Web Developer with a specialized
            certification in the MERN (MongoDB, Express.js, React, Node.js)
            stack. Bringing a wealth of hands-on experience, with 4-6 months
            experience, excelling in creative problem-solving and exceeding
            client expectations. Meticulous and adaptable, I bring high-quality
            results to every project. Ready to contribute my skills to your
            success! My commitment to continuous learning ensures that I stay at
            the forefront of industry trends, delivering innovative and
            efficient solutions.
          </Text>
        </Box>
      </Flex>
      <br></br>
      <Box maxW='200rem' paddingLeft='20px' paddingBottom='20px'>
        <Heading fontSize='3xl' paddingBottom='15px'>
          Skills:
        </Heading>
        <Flex padding={2} justifyContent='space-between'>
          <Box>
            <Image boxSize='5rem' src='assets/logo/htmllogojpg.jpg'></Image>
            <Text textAlign='center'>HTML</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/csslogo2.png'></Image>
            <Text textAlign='center'>CSS</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/javascriptlogo.png'></Image>
            <Text textAlign='center'>Javascript</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/mongodblogo.png'></Image>
            <Text textAlign='center'>MongoDB</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/expresslogo.png'></Image>
            <Text textAlign='center'>Express.js</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/reactlogopng.png'></Image>
            <Text textAlign='center'>React.js</Text>
          </Box>
          <Box>
            <Image boxSize='5rem' src='assets/logo/nodelogo.jpg'></Image>
            <Text textAlign='center'>Node.js</Text>
          </Box>
        </Flex>
      </Box>

      <Box>
        <Heading padding={3} fontSize='3xl'>
          My Projects
        </Heading>
        <SimpleGrid
          spacing={20}
          templateColumns='repeat(auto-fill, minmax(200px, 1fr))'>
          {/* PROJECT 1 */}
          <Card
            maxW='xl'
            bgColor='turquoise'
            _hover={{
              boxShadow: '1px 1px 10px 0.1px',
              transform: ' scale(1.05)',
              transition: 'transform 0.1s ease-in',
              bgColor: 'lightseagreen',
            }}>
            <CardBody>
              <Link as={RouterLink} to='/RstScreen'>
                <Image
                  src='assets\project_img\Rst\ProductsPage.png'
                  alt='RST Store Website Image '
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Heading size='md'>End-to-End Shopping Website</Heading>
                  <Text>
                    Online fashion haven, where style meets simplicity! Immerse
                    yourself in a curated collection of stunning clothing
                    products that redefine your wardrobe.
                  </Text>
                </Stack>
              </Link>
            </CardBody>
          </Card>

          {/* PROJECT 2 */}
          <Card
            maxW='xl'
            bgColor='turquoise'
            _hover={{
              boxShadow: '1px 1px 10px 0.1px',
              transform: ' scale(1.05)',
              transition: 'transform 0.1s ease-in',
              bgColor: 'lightseagreen',
            }}>
            <CardBody>
              <Link as={RouterLink} to='/SewingScreen'>
                <Image
                  src='assets\project_img\SewingProject\HomePage.png'
                  alt='Shop Website Image '
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Heading size='md'>Website of a Shop</Heading>
                  <Text>
                    Explore our handpicked selection of top-notch sewing
                    machines on our creativity today! From beginner-friendly
                    models to advanced options, find the perfect fit for your
                    projects.
                  </Text>
                </Stack>
              </Link>
            </CardBody>
          </Card>

          {/* PROJECT 3 */}
          <Card
            maxW='xl'
            bgColor='turquoise'
            _hover={{
              boxShadow: '1px 1px 10px 0.1px',
              transform: ' scale(1.05)',
              transition: 'transform 0.1s ease-in',
              bgColor: 'lightseagreen',
            }}>
            <CardBody>
              <Link as={RouterLink} to='/Portfolio'>
                <Image
                  src='assets\project_img\PortfolioProject\HomePage.png'
                  alt='PortFolio Website Image '
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Heading size='md'>PortFolio</Heading>
                  <Text>
                    My digital portfolio! Here, you'll find a curated collection
                    of my projects and skills alongside insightful information
                    about me
                  </Text>
                </Stack>
              </Link>
            </CardBody>
          </Card>

          {/* PROJECT 4 */}
          <Card
            maxW='xl'
            bgColor='turquoise'
            _hover={{
              boxShadow: '1px 1px 10px 0.1px',
              transform: ' scale(1.05)',
              transition: 'transform 0.1s ease-in',
              bgColor: 'lightseagreen',
            }}>
            <CardBody>
              <Link as={RouterLink} to='/LogicLoop'>
                <Image
                  src='assets\project_img\LogicLoop\logic1.png'
                  alt='LogicLoop '
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Heading size='md'>Landing Page</Heading>
                  <Text>
                    This is a Landing Page for the Universal High School
                    assigned as the task to make the complete landing page using
                    React.js
                  </Text>
                </Stack>
              </Link>
            </CardBody>
          </Card>

          {/* PROJECT 5 */}
          <Card
            maxW='xl'
            bgColor='turquoise'
            _hover={{
              boxShadow: '1px 1px 10px 0.1px',
              transform: ' scale(1.05)',
              transition: 'transform 0.1s ease-in',
              bgColor: 'lightseagreen',
            }}>
            <CardBody>
              <Link as={RouterLink} to='/CRUDScreen'>
                <Image
                  src='assets\project_img\CRUD_Project\CFirst.png'
                  alt='CRUD Website Image '
                  borderRadius='lg'
                />
                <Stack mt='6' spacing='3'>
                  <Heading size='md'>CRUD Operation</Heading>
                  <Text>
                    This is basic CRUD(Create, Read, Update, Delete) Operation.
                    It is a Form to add items in which you can add any item and
                    then update it also later delete the added item. It is
                    connected to the MongoDb Server which automatically saves
                    the data operation.
                  </Text>
                </Stack>
              </Link>
            </CardBody>
          </Card>
          {/*  */}
          {/* </Flex> */}
        </SimpleGrid>
      </Box>
    </>
  );
};

export default HomeScreen;
