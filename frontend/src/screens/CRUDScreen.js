import React from 'react';
import { Image, Box, Text, GridItem } from '@chakra-ui/react';

const CRUDScreen = () => {
  return (
    <div>
      <Text fontSize='40px' textAlign='center' padding={5}>
        <strong>CRUD Operation</strong>
      </Text>
      <Box display='grid' gridTemplateColumns='repeat(12, 1fr)' gap={2}>
        {/* Section 1 */}
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Form </strong>
          </GridItem>
          <Text>
            This is a Form to add the items in the list and store it. It also
            stores/saves the data in the MongoDB Database. It has a button to
            Add items, when clicked it will display a textbox to type the item
            name.
          </Text>
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CFirst.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        {/* Section 2 */}
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CSecond.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Add Data</strong>
          </GridItem>
          <Text>
            As the textbox appears you can add the data like names, numbers,
            etc. and click on the submit button (for eg. Coconut). The data will
            get add in the list below and also in the MongoDB Database.An Alert
            box will appear after adding data.
          </Text>
        </Box>

        {/* Section 3  */}
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Update Data</strong>
          </GridItem>
          <Text>
            Next if you want you can update the added data by the button given
            in front of the specific item name. Click on the update button and
            change the name in the textbox and click on submit. An alert
            dailogue box will appear when updated. Also the data will get
            updated in MongoDB.
          </Text>
        </Box>

        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CThird.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>

        {/* Section 4 */}
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CFourth.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Delete Data</strong>
          </GridItem>
          <Text>
            If you want you can also delete the added data by the button given
            in front of the specific item name. Click on the delete button and
            the data will be deleted and An alert dailogue box will appear when
            deleted. Also the data will get deleted in MongoDB.
          </Text>
        </Box>

        {/* Section 5  */}
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Final</strong>
          </GridItem>
          <Text>
            Here you can see the final list of the data saved in the list. And
            also in the MongoDB Database.
          </Text>
        </Box>

        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CFifth.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>

        {/* Section 6 */}
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\CRUD_Project\CSixth.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>MongoDB Data</strong>
          </GridItem>
          <Text>
            Here you can see the Data in the MongoDB Compass Databse and
            overview the saved data.
          </Text>
        </Box>
      </Box>
    </div>
  );
};

export default CRUDScreen;
