import React from 'react';
import { Image, Box, Text, GridItem } from '@chakra-ui/react';

const LogicLoop = () => {
  return (
    <div>
      <Text fontSize='40px' textAlign='center' padding={5}>
        <strong>Landing Page Using ReactJs</strong>
      </Text>
      <Box display='grid' gridTemplateColumns='repeat(12, 1fr)' gap={2}>
        {/* Section 1 Info */}
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Header and Form Section </strong>
          </GridItem>
          <Text>
            This landing page targets students in India considering entrance
            exams for IT or Medical fields between 2024-2026. It uses a clear
            call to action to "Enquire Now" and captures user information like
            name, phone number, and email address. This suggests it's designed
            to generate leads for a program called PACE, likely offering exam
            preparation courses
          </Text>
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\LogicLoop\logic1.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        {/* Section 2 */}
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\LogicLoop\logic3.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Tabs Section</strong>
          </GridItem>
          <Text>
            In this section, there are tabs section which contains different
            information and images according to the tabs including the
            information. Also there is a button which will navigate to the
            Enquire Form.
          </Text>
        </Box>
        {/* Section 3  */}
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Image Section </strong>
          </GridItem>
          <Text>
            The next section provides some images about the University. The
            activities, sports, and the different functions. It is like the
            showcase of the image gallery.
          </Text>
        </Box>

        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\LogicLoop\logic4.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>

        {/* Section 4 */}
        <Box gridColumn='span 6' padding='20px'>
          <Image
            src='assets\project_img\LogicLoop\logic2.png'
            alt='PortFolio Website Image '
            borderRadius='lg'
          />
        </Box>
        <Box gridColumn='span 6' padding='20px'>
          <GridItem fontSize='22px'>
            <strong>Footer Section </strong>
          </GridItem>
          <Text>
            This is the last section, the footer section. Footer contains the
            Map through which the client will get the current location of the
            University/Institute. Also it has the Address and the contact
            details for the University and the get in touch buttom which will
            take you to the enuire form.
          </Text>
        </Box>
      </Box>
    </div>
  );
};
export default LogicLoop;
